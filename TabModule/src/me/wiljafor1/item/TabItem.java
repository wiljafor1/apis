package me.wiljafor1.item;

import me.wiljafor1.util.Skin;

public interface TabItem {
    String getText();

    int getPing();

    Skin getSkin();

    boolean updateText();

    boolean updatePing();

    boolean updateSkin();

    boolean equals(Object object);
}
