package me.wiljafor1.item;

import me.wiljafor1.util.Skin;
import me.wiljafor1.util.Skins;

public class BlankTabItem extends TextTabItem {
    public BlankTabItem(Skin skin) {
        super("", 1000, skin);
    }

    public BlankTabItem() {
        this(Skins.DEFAULT_SKIN);
    }
}
