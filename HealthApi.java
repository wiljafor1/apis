import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;

import com.google.common.collect.Maps;

public class HealthApi implements Listener{
	/* By Wiljafor1 Deixa Creditos ao usar                           */
	/* Para usar baste apenas registrar o evento da api na main e colocar isso no OnJoin
	 * 
	 * HealthApi r = HealthApi.getInstance();
	 * 
	 * r.CreateOne(PlayerAqui, new Double[] {VidaAtual,VidaMaxima});
	 * 
	 * Exemplo: r.CreateOne(p, new Double[] {100.0,100.0});
	 */
	public static Map<Player, Double[]> playerdata = Maps.newHashMap();
	public void CreateOne(Player player,Double vida[]) {
		playerdata.put(player, vida);
	}
	
	public static HealthApi getInstance() {
		return new HealthApi();
	}
	
	public void remove(Player p) {
		playerdata.remove(p);
	}
	
	public Double getVida(Player p) {
		Double[] vida = playerdata.get(p);
		return vida[0];
	}
	
	public Double getVidaMax(Player p) {
		Double[] vida = playerdata.get(p);
		return vida[1];
	}
	
	public void setVida(Player p,Double vida) {
		Double[] vidafinal = new Double[] {
			vida,getVidaMax(p)
		};
		playerdata.put(p, vidafinal);
	}
	
	public void setVidaMax(Player p,Double vida) {
		Double[] vidafinal = new Double[] {
				getVida(p),vida
		};
		playerdata.put(p, vidafinal);
	}
	/*                        Eventos                            */

	@EventHandler
	public void onPlayerDamage(EntityDamageEvent e) {
		if (e.getEntity() instanceof Player) {
			Player p = (Player) e.getEntity();
			if(j.getCurrentAccount() != null) {
				if (e.getCause() == DamageCause.FALL) {
					double vida = getVida(p) - 2.5 * p.getFallDistance();
					if (vida <= 0) {
						p.setHealth(0.5);
						e.setDamage(0.5);
						setVida(p, getVidaMax(p));
					} else {
						p.setHealth(20);
						setVida(p, vida);
						e.setDamage(0.0);
					}
				} else {
					double vida = j.getCurrentAccount().getVida() - e.getDamage();
					if (vida <= 0) {
						p.setHealth(0.5);
						e.setDamage(0.5);
						setVida(p, getVidaMax(p));
					} else {
						p.setHealth(20);
						setVida(p, vida);
						e.setDamage(0.0);
					}
				}
			}
			else {
				e.setCancelled(true);
			}
		}
	}
}